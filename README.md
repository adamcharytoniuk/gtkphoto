# On Mac

first, install gtk

    brew install gtk+3

set the env variables for `libffi` to be properly recognised

```
export LDFLAGS="-L/usr/local/opt/libffi/lib"
export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"
```

then compile the project:

    cargo build

Install glade.

    brew install glade


# On Linux Ubuntu 18.04

Install additional packages:
    
    sudo apt install libgtk-3-dev

the build project

    cargo build

Then install glade application:

    sudo apt install glade
