#![windows_subsystem = "windows"]


extern crate gdk_pixbuf;
extern crate gio;
extern crate glib;
extern crate gtk;
use gio::prelude::*;
use gtk::prelude::*;

use std::cell::RefCell;
use std::env;

mod action;
mod error;
use error::AppError;
mod static_resources;

use crate::action::ActionCollection;
use crate::action::Document;
use crate::action::DocumentImpl;

/// Binds button click event with an action from action collection
/// 
/// # Arguments
/// - `builder` a reference to the [builder](gtk::Builder) used to find the button
/// - `button_id` the string id of the button in glade UI file.
/// - `action_collection` the shared reference to an action collection 
/// - `action_name` a name of the action as given in the action collection
/// - `document` - a shareable reference to the document cell
fn bind_button_clicked(
    builder: &gtk::Builder,
    button_id: &str,
    action_collection: &std::rc::Rc<ActionCollection<DocumentImpl>>,
    action_name: &str,
    document: &std::rc::Rc<RefCell<DocumentImpl>>,
) {
    let document_clone = std::rc::Rc::clone(&document);
    let action_collection_clone = std::rc::Rc::clone(&action_collection);
    let button: gtk::ToolButton = builder.get_object(button_id).unwrap();
    let action_name_string = action_name.to_string();
    button.connect_clicked(move |_btn| {
        let mut d = document_clone.borrow_mut();
        match action_collection_clone.run_action(&action_name_string, &mut d) {
            Ok(_) => (),
            Err(e) => println!("ERROR: {:?}", e),
        };
    });
}

fn main() {
    static_resources::init();

    let uiapp = gtk::Application::new(
        Some("org.incred.gtkphoto"),
        gio::ApplicationFlags::FLAGS_NONE,
    )
    .expect("Application::new failed");

    // Then we call the Builder call.
    let builder = gtk::Builder::new_from_resource("/mainwindow.glade");

    uiapp.connect_activate(move |app| {
        let close_image_action = action::RelayAction {
            execute_func: |document: &mut action::DocumentImpl| document.reset(),
            can_execute: |_document: &mut action::DocumentImpl| true, // this action is available by default
        };

        let open_file_action = action::RelayAction {
            execute_func: move |document: &mut action::DocumentImpl| {
                let dialog = gtk::FileChooserDialog::with_buttons::<gtk::Window>(
                    Some("Open File"),
                    None,
                    gtk::FileChooserAction::Open,
                    &[
                        ("_Cancel", gtk::ResponseType::Cancel),
                        ("_Open", gtk::ResponseType::Accept),
                    ],
                );

                let dialog_res = dialog.run();
                let res: Result<(), AppError> = match dialog_res {
                    gtk::ResponseType::Accept => {
                        let filename = dialog.get_filename().ok_or(AppError::NoFileSelected)?;
                        let pb = gdk_pixbuf::Pixbuf::new_from_file(filename)?;
                        document.set_pixmap(Some(pb))
                    }
                    _ => Ok(()),
                };
                dialog.destroy();
                res
            },
            can_execute: |_document: &mut action::DocumentImpl| true, // always on ?
        };

        let zoom_in_action = action::RelayAction {
            execute_func: move |document: &mut action::DocumentImpl| document.scale_by(1.25),
            can_execute: move |_document: &mut action::DocumentImpl| true,
        };
        let zoom_out_action = action::RelayAction {
            execute_func: move |document: &mut action::DocumentImpl| document.scale_by(0.8f64),
            can_execute: move |_document: &mut action::DocumentImpl| true,
        };

        let mut action_collection = action::ActionCollection::<DocumentImpl>::new();
        action_collection.actions.insert(
            "close_image_action".to_string(),
            Box::new(close_image_action),
        );
        action_collection
            .actions
            .insert("open_file_action".to_string(), Box::new(open_file_action));
        action_collection
            .actions
            .insert("zoom_in_action".to_string(), Box::new(zoom_in_action));
        action_collection
            .actions
            .insert("zoom_out_action".to_string(), Box::new(zoom_out_action));

        let action_collection_shared = std::rc::Rc::new(action_collection);

        let window: gtk::ApplicationWindow = builder.get_object("mainwindow").unwrap();
        window.set_application(Some(app));

        let document = std::rc::Rc::new(RefCell::new(action::DocumentImpl::new(
            builder.get_object("image").unwrap(),
            builder.get_object("fixed_layout").unwrap(),
        )));

        bind_button_clicked(
            &builder,
            "open_image",
            &action_collection_shared,
            "open_file_action",
            &document,
        );

        bind_button_clicked(
            &builder,
            "zoom_in",
            &action_collection_shared,
            "zoom_in_action",
            &document,
        );
        bind_button_clicked(
            &builder,
            "zoom_out",
            &action_collection_shared,
            "zoom_out_action",
            &document,
        );
        bind_button_clicked(
            &builder,
            "file_close",
            &action_collection_shared,
            "close_image_action",
            &document,
        );
        {
            let autosize_button: gtk::ToolButton = builder.get_object("zoom_auto").unwrap();
            let document_clone = std::rc::Rc::clone(&document);
            let builder_clone = builder.clone();
            autosize_button.connect_clicked(move |_btn| {
                let scroll_view: gtk::ScrolledWindow =
                    builder_clone.get_object("scrolled_window").unwrap();

                let rect = scroll_view.get_allocation();
                let (w, h) = (rect.width, rect.height);
                match document_clone.borrow_mut().scale_image_to(w, h) {
                    Ok(_) => (),
                    Err(e) => println!("ERROR: {:?}", e),
                }
            });
        }

        {
            let brighten_button: gtk::ToggleToolButton =
                builder.get_object("view_mode_light").unwrap();
            let document_clone = std::rc::Rc::clone(&document);
            brighten_button.connect_clicked(move |btn| {
                let is_active = btn.get_active();
                match document_clone.borrow_mut().set_brighten(is_active) {
                    Ok(_) => (),
                    Err(e) => println!("ERROR: {:?}", e),
                }
            });
        }

        {
            // connecting to mouse click events
            let event_box: gtk::EventBox = builder.get_object("image_event_box").unwrap();
            // let event_box: gtk::EventBox = builder.get_object("image_event_box").unwrap();
            let document_clone = std::rc::Rc::clone(&document);
            // let builder_clone = std::rc::Rc::clone(&document);
            event_box.set_events(gdk::EventMask::BUTTON_PRESS_MASK);
            event_box.connect_button_press_event(move |_eb, e| {
                // only left-mouse button handled
                if e.get_button() == 1 {
                    let res = document_clone.borrow_mut().mouse_clicked(&e.get_position());
                    match res {
                        Ok(_) => (),
                        Err(e) => println!("ERROR: {:?}", e),
                    }
                }
                gtk::Inhibit(true)
            });
        }

        window.show_all();
    });

    // We start the gtk main loop.
    uiapp.run(&env::args().collect::<Vec<_>>());
}
