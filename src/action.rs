use super::error::AppError;
use crate::gdk_pixbuf;
use crate::gtk::ContainerExt;
use crate::gtk::FixedExt;
use crate::gtk::ImageExt;
use crate::gtk::WidgetExt;
use std::collections::HashMap;

pub trait Document {
    fn set_pixmap(&mut self, pixbuf: Option<gdk_pixbuf::Pixbuf>) -> Result<(), AppError>;
    fn get_scale(&self) -> f64;
    fn scale_image_to(&mut self, w: i32, h: i32) -> Result<(), AppError>;
    fn set_brighten(&mut self, enable: bool) -> Result<(), AppError>;
    fn scale_by(&mut self, factor: f64) -> Result<(), AppError>;
    fn mouse_clicked(&mut self, point: &(f64, f64)) -> Result<(), AppError>;
}

pub trait Action<D>
where
    D: Document,
{
    fn execute(&self, d: &mut D) -> Result<(), AppError>;
    fn is_enabled(&self, d: &mut D) -> bool;
}

pub struct ActionCollection<D>
where
    D: Document,
{
    pub actions: HashMap<String, Box<dyn Action<D>>>,
}

impl<D> ActionCollection<D>
where
    D: Document,
{
    pub fn new() -> ActionCollection<D> {
        ActionCollection {
            actions: HashMap::new(),
        }
    }
    pub fn run_action(&self, name: &str, d: &mut D) -> Result<(), AppError> {
        let action_name_string = name.to_string();
        let action = self
            .actions
            .get(&action_name_string)
            .ok_or(AppError::ActionNotFound(action_name_string))?;
        action.execute(d)
    }
}

pub struct RelayAction<T, P> {
    pub execute_func: T,
    pub can_execute: P,
}

impl<D, T, P> Action<D> for RelayAction<T, P>
where
    D: Document,
    T: Fn(&mut D) -> Result<(), AppError>,
    P: Fn(&mut D) -> bool,
{
    fn execute(&self, d: &mut D) -> Result<(), AppError> {
        (self.execute_func)(d)
    }
    fn is_enabled(&self, d: &mut D) -> bool {
        (self.can_execute)(d)
    }
}

pub struct FaceMask {
    x: i32,
    y: i32,
}

pub struct DocumentImpl {
    pub image: gtk::Image,
    pub fixed_layout: gtk::Fixed,
    pub orig_pixbuf: Option<gdk_pixbuf::Pixbuf>,
    pub scale: f64,
    pub brightened: bool,
    pub face_masks: Vec<FaceMask>,
}

/// Finds and returns an optimal scale that will allow to resize original pixbuf to fit into given window size.
/// # Arguments:
/// - `window_size` a size of the window in pixels
/// - `image_size` a size of the image loaded
/// 
/// # Examples
/// ```
/// use action;
/// let scale = find_optimal_scale(&(128,128),&(64,64));
/// assert_eq!(scale, 2020.0);
/// ```
pub fn find_optimal_scale(window_size: &(i32, i32), image_size: &(i32, i32)) -> f64 {
    let ws = (window_size.0 as f64, window_size.1 as f64);
    let is = (image_size.0 as f64, image_size.1 as f64);
    let ratio_w = ws.1 / ws.0;
    let ratio_i = is.1 / is.0;
    if ratio_w > ratio_i {
        100.0 * ws.0 / is.0
    } else {
        100.0 * ws.1 / is.1
    }
}

impl DocumentImpl {
    /// Creates and returns an empty document.
    /// 
    /// # Arguments
    /// * `image` - an [image](gtk::Image) widget to use for UI updates later on
    /// * `fixed_layout` - a [fixed layout](gtk::Fixed) container in UI to use to place face masks onto
    pub fn new(image: gtk::Image, fixed_layout: gtk::Fixed) -> DocumentImpl {
        let mut doc = DocumentImpl {
            image: image,
            fixed_layout: fixed_layout,
            orig_pixbuf: None,
            scale: 25.0,
            brightened: false,
            face_masks: vec![],
        };
        let _ = doc.update_image();
        doc
    }

    /// Restores document to an empty state
    /// 
    /// Any previously loaded image as well any added [face masks](action::FaceMask) are lost.
    pub fn reset(&mut self) -> Result<(), AppError> {
        let _ = self.face_masks.clear();
        self.scale = 25.0;
        self.set_pixmap(None)
    }

    /// This is just a utility function to get a smaller version of original image.
    fn get_scaled_pixbuf(
        pb: &gdk_pixbuf::Pixbuf,
        scale: f64,
        alpha: i32,
    ) -> Result<gdk_pixbuf::Pixbuf, AppError> {
        let pixbuf = pb;
        let divider = 100.0 / scale;
        let w: f64 = pixbuf.get_width() as f64;
        let h: f64 = pixbuf.get_height() as f64;
        let ww: i32 = (w / divider) as i32;
        let hh: i32 = (h / divider) as i32;
        let scaled_pixbuf = pixbuf.composite_color_simple(
            ww,
            hh,
            gdk_pixbuf::InterpType::Bilinear,
            alpha,
            32,
            0xFFFFFF,
            0xFFFFFF,
        );
        scaled_pixbuf.ok_or(AppError::CannotCreateScaledCompositePixbuf)
    }

    /// Updates UI to reflect latest changes in the document
    /// 
    /// This function is internally called when document is modified through its trait method implementation.
    fn update_image(&mut self) -> Result<(), AppError> {
        let alpha = match self.brightened {
            true => 96,
            false => 255,
        };

        // get all children and remove the ones that need re-positioning
        // TODO: optimize this for doing it only when position needs changing
        self.fixed_layout
            .get_children()
            .iter()
            // skip the image one
            .filter(|c| *c != &self.image)
            // remove those children
            .for_each(|c| self.fixed_layout.remove(c));

        let pixmap = self
            .orig_pixbuf
            .as_ref()
            .and_then(|pixbuf| {
                // returned scaled and potentially brightened pixbuf version
                DocumentImpl::get_scaled_pixbuf(&pixbuf, self.scale, alpha).ok()
            })
            .or(
                //  or an application logo
                gdk_pixbuf::Pixbuf::new_from_resource_at_scale("/face-logo.svg", 600, 300, true)
                    .ok(),
            );
        self.image.set_from_pixbuf(pixmap.as_ref());

        let scale_factor = self.scale / 100.0;
        self.face_masks.iter().for_each(|face_mask| {
            let (x, y) = (face_mask.x, face_mask.y);
            // find new position after scale
            let xx = ((x as f64) * scale_factor) as i32;
            let yy = ((y as f64) * scale_factor) as i32;
            // and add the widget to the layout
            let (l, w, h) = self.create_face_widget();
            self.fixed_layout.put(
                &l,
                xx - (((w as f64) / 2.0) as i32),
                yy - (((h as f64) / 2.0) as i32),
            );
            l.show();
        });

        Ok(())
    }

    fn create_face_widget(&self) -> (gtk::Image, i32, i32) {
        let factor = self.scale / 100.;
        const DEFAULT_FACE_SIZE: f64 = 128.0;
        let p = gdk_pixbuf::Pixbuf::new_from_resource_at_scale(
            "/face.svg",
            (DEFAULT_FACE_SIZE * factor) as i32,
            (DEFAULT_FACE_SIZE * factor) as i32,
            true,
        )
        .expect("cannot load pixmap face icon from a file");
        let (w, h) = (p.get_width(), p.get_height());
        let l = gtk::Image::new_from_pixbuf(Some(p).as_ref());
        (l, w, h)
    }
}

impl Document for DocumentImpl {
    fn set_pixmap(&mut self, new_pb: Option<gdk_pixbuf::Pixbuf>) -> Result<(), AppError> {
        self.orig_pixbuf = new_pb;
        self.update_image()
    }

    fn get_scale(&self) -> f64 {
        self.scale
    }
    fn scale_image_to(&mut self, w: i32, h: i32) -> Result<(), AppError> {
        if let Some(pixbuf) = &self.orig_pixbuf {
            let iw = pixbuf.get_width();
            let ih = pixbuf.get_height();
            self.scale = find_optimal_scale(&(w, h), &(iw, ih));;
            self.update_image()
        } else {
            Err(AppError::CannotScalePixbufNotSet)
        }
    }
    fn set_brighten(&mut self, enable: bool) -> Result<(), AppError> {
        self.brightened = enable;
        self.update_image()
    }

    fn scale_by(&mut self, factor: f64) -> Result<(), AppError> {
        self.scale = self.scale * factor;
        self.update_image()
    }

    fn mouse_clicked(&mut self, point: &(f64, f64)) -> Result<(), AppError> {
        let pixbuf_size = self
            .orig_pixbuf
            .as_ref()
            .map(|p| (p.get_width(), p.get_height()));
        let c = find_orig_coords(point, self.scale, pixbuf_size, &self.face_masks)?;
        match c {
            OrigCoords::InBounds(x, y) => {
                let face_mask = FaceMask { x: x, y: y };
                self.face_masks.push(face_mask);
                self.update_image()
            }
            OrigCoords::OutsideBounds => Ok(()),
            OrigCoords::InFaceMask(_x, _y) => Ok(()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum OrigCoords {
    InBounds(i32, i32),
    InFaceMask(i32, i32),
    OutsideBounds,
}

fn find_orig_coords(
    point: &(f64, f64),
    scale: f64,
    pixbuf_size: Option<(i32, i32)>,
    face_masks: &Vec<FaceMask>,
) -> Result<OrigCoords, AppError> {
    if let Some(pixbuf_size) = pixbuf_size {
        let scale_factor = scale / 100.;
        let displayed_pb_w = (pixbuf_size.0 as f64) * scale_factor;
        let displayed_pb_h = (pixbuf_size.1 as f64) * scale_factor;
        if point.0 > displayed_pb_w || point.1 > displayed_pb_h {
            return Ok(OrigCoords::OutsideBounds);
        }

        if let Some(clicked_face) = face_masks.iter().find(|fc| {
            let (x, y) = (fc.x, fc.y);
            ((x as f64) * scale_factor - point.0).abs() < 64. * scale_factor
                && ((y as f64) * scale_factor - point.1).abs() < 64. * scale_factor
        }) {
            return Ok(OrigCoords::InFaceMask(clicked_face.x, clicked_face.y));
        }

        Ok(OrigCoords::InBounds(
            (point.0 / scale_factor) as i32,
            (point.1 / scale_factor) as i32,
        ))
    } else {
        Err(AppError::CannotDetermineOrigCoordsNoPixbufSize)
    }
}

#[cfg(test)]
mod unittesting {
    use super::super::error::AppError;
    use super::FaceMask;
    use super::OrigCoords;
    use assert_approx_eq::assert_approx_eq;

    #[test]
    fn find_orig_coords_called_with_empty_pixbuf_size_returns_err() {
        let result = super::find_orig_coords(&(16., 16.), 100., None, &Vec::new());
        assert_eq!(Err(AppError::CannotDetermineOrigCoordsNoPixbufSize), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_inside_of_pixbuf_bounds_returns_inbounds() {
        let result = super::find_orig_coords(&(4., 8.), 100., Some((16, 16)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(4, 8)), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_inside_of_pixbuf_bounds_and_inside_face_masks_returns_in_face(
    ) {
        let result = super::find_orig_coords(
            &(4., 8.),
            100.,
            Some((16, 16)),
            &vec![FaceMask { x: 4, y: 8 }],
        );
        assert_eq!(Ok(OrigCoords::InFaceMask(4, 8)), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_outside_of_pixbuf_bounds_2_returns_outsidebounds() {
        let result = super::find_orig_coords(&(18., 4.), 100., Some((16, 16)), &Vec::new());
        assert_eq!(Ok(OrigCoords::OutsideBounds), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_outside_of_pixbuf_bounds_3_returns_outsidebounds() {
        let result = super::find_orig_coords(&(31., 16.), 100., Some((16, 16)), &Vec::new());
        assert_eq!(Ok(OrigCoords::OutsideBounds), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_outside_of_pixbuf_bounds_4_returns_outsidebounds() {
        let result = super::find_orig_coords(&(16., 31.), 100., Some((16, 16)), &Vec::new());
        assert_eq!(Ok(OrigCoords::OutsideBounds), result);
    }
    #[test]
    fn find_orig_coords_called_with_point_outside_of_downscaled_pixbuf_bounds_returns_outsidebounds(
    ) {
        let result = super::find_orig_coords(&(4., 4.), 50., Some((32, 32)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(8, 8)), result);
    }
    #[test]
    fn find_orig_coords_called_with_pixbuf_size_same_as_viewport_returns_values_same_as_point() {
        let result = super::find_orig_coords(&(16., 16.), 100., Some((32, 32)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(16, 16)), result);
    }
    #[test]
    fn find_orig_coords_called_with_pixbuf_size_twice_as_viewport_returns_values_the_point() {
        let result = super::find_orig_coords(&(8., 16.), 100., Some((64, 64)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(8, 16)), result);
    }
    #[test]
    fn find_orig_coords_called_with_pixbuf_size_half_as_viewport_returns_values_half_the_point() {
        let result = super::find_orig_coords(&(14., 14.), 100., Some((16, 16)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(14, 14)), result);
    }
    #[test]
    fn find_orig_coords_called_with_pixbuf_size_same_as_viewport_but_upscaled_2x_returns_values_half_the_point(
    ) {
        let result = super::find_orig_coords(&(16., 16.), 200., Some((32, 32)), &Vec::new());
        assert_eq!(Ok(OrigCoords::InBounds(8, 8)), result);
    }

    #[test]
    fn find_optimal_scale_called_with_same_values_returns_100() {
        let result = super::find_optimal_scale(&(32, 45), &(32, 45));
        assert_approx_eq!(100.0, result, 0.001);
    }
    #[test]
    fn find_optimal_scale_called_with_window_size_ten_times_the_picture_returns_1000() {
        let result = super::find_optimal_scale(&(320, 450), &(32, 45));
        assert_approx_eq!(1000.0, result, 0.001);
    }
    #[test]
    fn find_optimal_scale_called_with_window_size_tenth_of_the_picture_returns_10() {
        let result = super::find_optimal_scale(&(32, 45), &(320, 450));
        assert_approx_eq!(10.0, result, 0.001);
    }

    #[test]
    fn find_optimal_scale_called_with_window_rect_and_picture_vert_rect_returns_100() {
        let result = super::find_optimal_scale(&(50, 50), &(10, 50));
        assert_approx_eq!(100.0, result, 0.001);
    }
    #[test]
    fn find_optimal_scale_called_with_window_rect_and_picture_smaller_vert_rect_returns_200() {
        let result = super::find_optimal_scale(&(50, 50), &(10, 25));
        assert_approx_eq!(200.0, result, 0.001);
    }
    #[test]
    fn find_optimal_scale_called_with_window_rect_and_picture_horiz_rect_returns_100() {
        let result = super::find_optimal_scale(&(50, 50), &(50, 10));
        assert_approx_eq!(100.0, result, 0.001);
    }
    #[test]
    fn find_optimal_scale_called_with_window_rect_and_picture_smaller_horiz_rect_returns_200() {
        let result = super::find_optimal_scale(&(50, 50), &(25, 10));
        assert_approx_eq!(200.0, result, 0.001);
    }
}
