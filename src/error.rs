


#[derive(Debug)]
#[derive(PartialEq)]
pub enum AppError {
    GtkError(gtk::Error),
    // ScalingNotEnoughMemoryError,
    ActionNotFound(String),
    NoFileSelected,
    CannotDetermineOrigCoordsNoPixbufSize,
    CannotScalePixbufNotSet,
    CannotCreateScaledCompositePixbuf,
}

impl std::convert::From<gtk::Error> for AppError {
    fn from(e: gtk::Error) -> AppError {
        AppError::GtkError(e)
    }
}
